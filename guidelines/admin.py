from django.contrib import admin
from .models import Article, Category

@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
	list_display = ('title', 'tagline', 'category')
	search_field = ('title', 'tagline', 'category', 'text')
	prepopulated_fields = {'tagline':('title',)}
	
@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
	list_display = ('name', 'tagline')
	search_field = ('name', 'tagline')
	prepopulated_fields = {'tagline':('name',)}
