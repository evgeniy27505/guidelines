from __future__ import unicode_literals
from django.db import models
from redactor.fields import RedactorField
from taggit.managers import TaggableManager

class Category(models.Model):
	name = models.CharField("Название", max_length=200)
	tagline = models.CharField("Слоган", max_length=200)
	def __unicode__(self):
		return self.name
	class Meta:
		verbose_name = "Категория"
		verbose_name_plural = "Категории"

class Article(models.Model):
	title = models.CharField("Название", max_length=200)
	tagline = models.CharField("Слоган", max_length=200)
	text = RedactorField(verbose_name="Текст")
	category = models.ForeignKey(Category, verbose_name="Категория", related_name="articles")
	tags = TaggableManager(verbose_name="Теги")
	def __unicode__(self):
		return self.title
	class Meta:
		verbose_name = "Статья"
		verbose_name_plural = "Статьи"