from django.shortcuts import render
from .models import Article

def articles_list(request):
	articles = Article.objects.filter()
	return render(request,'guidelines/articles.html', {'articles':articles})